\documentclass[11pt]{beamer}
\usetheme{Dresden}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{amsmath, bm}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepackage{etoolbox} 
\usepackage{listofitems} 
\usepackage{pgfgantt}
\usepackage{outlines}
\usepackage{blindtext}

\author{Huu Nhat Huynh}
\title{Model-based Deep Learning \newline for Signal Processing and Telecommunication}
%\setbeamercovered{transparent} 
\setbeamertemplate{navigation symbols}{} 
%\logo{\includegraphics[scale=0.08]{figures/logo-imt-atlantique.jpg}} 
\institute{IMT Atlantique} 
\date{24 May 2024} 
%\subject{} 

\addtobeamertemplate{navigation symbols}{}{%
    \usebeamerfont{footline}%
    \usebeamercolor[fg]{footline}%
    \hspace{1em}%
    \insertframenumber/\inserttotalframenumber
}

\newcommand\blfootnote[1]{%
\begingroup
\renewcommand\thefootnote{}\footnote{#1}%
\addtocounter{footnote}{-1}%
\endgroup
}

\begin{document}


\begin{frame}
\titlepage
\end{frame}


\begin{frame}{Outline}
    \begin{outline}
        \1 PhD objective
        \1 Quick review of
        \2 signal modeling for array signal processing
        \2 MUltiple SIgnal Classification (MUSIC) algorithm
        \2 neural networks
        \1 Main results
        \1 Future research directions
        \1 Work plan
        \1 Training courses
    \end{outline}
\end{frame}


\begin{frame}{PhD Objective}

Model-based deep learning: hybrid model combining classical algorithms and deep learning \newline

My objective is to design hybrid models that possibly
\begin{itemize}
	\item perform better than classical algorithms
	\item with lower computational cost
	\item are more interpretable than deep neural networks %why?, 
	\item require less training data than deep neural networks \newline %why?
\end{itemize}  

\end{frame}

\begin{frame}{PhD Objective}

Application domains
\begin{itemize}
	\item Array signal processing: Direction of Arrival (DoA) estimation for static sources (first year) and dynamic sources (upcoming year)
	\item Wireless communications: Channel estimation and equalization (upcoming year)
\end{itemize}

\end{frame}


\begin{frame}{Quick review of array signal processing}
\framesubtitle{Signal model}

\begin{columns}
 \begin{column}{.55\textwidth}
Signal equation:
\[\bm{x}(t) = \bm{A}(\bm{\theta})\bm{s}(t) + \bm{n}(t)\]

With \(\bm{x}(t) \in \mathbb{C}^{M}\) the observed signal, \(\bm{s}(t) \in \mathbb{C}^{D}\) the transmitted signal, \(\bm{A}(\bm{\theta})=\left[\bm{a}(\theta_{1}) \quad \bm{a}(\theta_{2}) \quad ... \quad \bm{a}(\theta_{D})\right]\) the steering matrix, and \(\bm{n}(t) \in \mathbb{C}^{M}\) additive white Gaussian noise 
 \end{column}

 \begin{column}{.49\textwidth}
\begin{figure}
    \centering
    \resizebox{1.0\linewidth}{!}{\input{figures/array.tex}}
\end{figure}  
 \end{column}
\end{columns}

\end{frame}


\begin{frame}{Quick review of array signal processing}
\framesubtitle{Signal model}

For an uniform linear array, steering vector \(\bm{a}(\theta)\) is  \[\bm{a}(\theta) = \left[1 \quad \omega \quad \omega^{2} \quad ... \quad \omega^{M-1}\right]^T\]
With \[\omega = exp\left(-j2\pi\frac{d \, sin(\theta)}{\lambda}\right)\] Where \(d\) is the sensor distance and \(\lambda\) is the wavelenght

\end{frame}


\begin{frame}{Quick review of array signal processing}
\framesubtitle{MUSIC - A Classical Algorithm}

\begin{itemize}

\item Compute estimated covariance matrix \[\bm{\hat{R}_{x}}=\frac{1}{T}\sum_{t=1}^{T}\bm{x}(t)\bm{x}(t)^H\]

\item Compute eigenvalue decomposition \(\bm{\hat{R}_{x}}=\bm{UDU}^H\)

\item Let \(\bm{U_{N}}\) be the subspace corresponding to \(M-D\) smallest eigenvalues. Compute the pseudo-spectrum over a discretized grid of \(\theta\) \[P(\theta)=\frac{1}{\|\bm{U_{N}}^{H}\bm{a}(\theta)\|^{2}}\] 

\item Select \(D\) values of \(\theta\) corresponding to \(D\) highest peaks of the pseudo-spectrum

\end{itemize}

\end{frame}


\begin{frame}{Quick review of neural networks}
\framesubtitle{Multilayer Perceptron}

\begin{figure}
    \centering
    \resizebox{0.6\linewidth}{!}{\input{figures/mlp.tex}}
    \caption{Multilayer Perceptron (MLP)}
\end{figure}   

\end{frame}


%\begin{frame}{Quick review of neural networks}
%\framesubtitle{Convolutional Neural Networks (CNN)}
%
%\end{frame}


\begin{frame}{Quick review of neural networks}
\framesubtitle{Recurrent Neural Networks (RNN)}

\begin{figure}
    \centering
    \includegraphics[scale=0.25]{figures/rnn.png}
    \caption{Recurrent Neural Network}
\end{figure} 

Some types of RNN: vanilla RNN, LSTM, GRU, etc.

\end{frame}


\begin{frame}{Main results}
\framesubtitle{Deep Augmented MUSIC \(^{[1]}\)}

\begin{figure}
    \centering
    \resizebox{1.0\linewidth}{!}{\input{figures/da-music.tex}}
    \caption{Deep Augmented MUSIC (DA-MUSIC)}
\end{figure}   

\blfootnote{\tiny \(^{[1]}\) Merkofer et al., Deep Augmented Music Algorithm for Data-Driven Doa Estimation, International Conference on Acoustics, Speech and Signal Processing 2022}

\end{frame}

\begin{frame}{Main results}
\framesubtitle{Modified DA-MUSIC}

Motivation: 

\begin{outline}
 \1 DA-MUSIC provides a surrogate covariance matrix that 
   \2 possibly accounts for wideband signals, array irregularities, ...
   \2 but it is neither positive nor Hermitian
\end{outline}

Proposal: 

\begin{itemize}
	\item Add a transformation \(\bm{X} \rightarrow \bm{X}\bm{X}^{H}\) before taking the eigenvalue decomposition
\end{itemize}

Benefits: 

\begin{itemize}
	\item Speed up the computation time
	\item Make the model more interpretable
\end{itemize}

\end{frame}


\begin{frame}{Main results}
\framesubtitle{Proposed data-driven model}

\begin{figure}
    \centering
    \resizebox{0.6\linewidth}{!}{\input{figures/proposed-model.tex}}
    \caption{Proposed data-driven model}
\end{figure}    
    
\end{frame}


\begin{frame}{Main results \(^{[2]}\)}

\begin{figure}
    \centering
    \includegraphics[scale=0.4]{figures/compare_perf.png}
    \caption{Experimental results for $T=200$, $M=8$ and $D=3$}
\end{figure} 

\blfootnote{\tiny \(^{[2]}\) Huynh H.N. et al. (2024). Revisiting Deep Augmented MUSIC Algorithm, submitted to Asilomar Conference on Signals, Systems, and Computers 2024}

\end{frame}


%\begin{frame}{Main results}
%\framesubtitle{Maximum Likelihood penalization}
%    
%\end{frame}


\begin{frame}{Future research directions}

\begin{outline}
\1 DoA estimation:
	\2 Model order estimation
	\2 Estimation of moving sources (DoA tracking)
	\2 State Space Model (angles, signals): KalmanNet \newline
\1 Channel estimation and equalization:
	\2 static source
	\2 moving source
	\2 several sources (MIMO)
\end{outline}

\end{frame}

\begin{frame}{Work plan}

\scriptsize

\begin{itemize}
	\item Task 1: Performance evaluation in non-ideal conditions 
	\item Task 2: DoA estimation for dynamic sources
	\item Task 3: Bibliographic studies on classical and modern algorithms for channel estimation and equalization 
	\item Task 4: Development of hybrid models for static channel identification 
	\item Task 5: Development of hybrid models for channel estimation and equalization for moving source, multiple sources
\end{itemize}

\begin{ganttchart}[
    hgrid,
    vgrid,
    x unit=0.13cm,
    y unit title=0.4cm,
    y unit chart=0.4cm,
    title height=1,
    title label font=\bfseries\footnotesize,
    bar/.append style={fill=blue!50},
    bar label font=\footnotesize
    ]{1}{72}
    
    \gantttitle{2024}{24} \gantttitle{2025}{48} \\ 
    \gantttitlelist{7,...,12}{4}  \gantttitlelist{1,...,12}{4}\\
    \ganttbar{Task 1}{2}{3} \\
    \ganttbar{Task 2}{4}{16} \\
    \ganttbar{Task 3}{17}{22} \\
    \ganttbar{Task 4}{23}{48} \\ 
    \ganttbar{Task 5}{49}{72}
\end{ganttchart}

\end{frame}


\begin{frame}{Training courses}

Finished courses (32 hours in total)
\begin{itemize}
	\item Opening day for PhD students (5 hours)
	\item Foundation models for speech, vision, and text (5 hours)
	\item English writing for scientific publication (15 hours)
	\item Julia programming language for scientific computing (4 hours)
	\item Bibliography management with Zotero (3 hours)
\end{itemize}

To enroll in next year (68 hours in total)
\begin{itemize}
	\item Research ethics and scientific integrity
	\item Ecological and societal issues
	\item Awareness of harassment
	\item etc.
\end{itemize}

\end{frame}


\begin{frame}{References}

\scriptsize

Data-driven models:
\begin{itemize}
	\item Liu et al. (2018). Direction-of-arrival estimation based on deep neural networks with robustness to array imperfections. IEEE Transactions on Antennas and Propagation
	% In the absence of array imperfections, the DNN-based method does not outperform traditional methods like MUSIC, which might limit its appeal in environments where such imperfections are minimal or well-calibrated​. The DNN framework necessitates a substantial amount of labeled training data, which may be difficult to obtain in practical scenarios.
	\item G. K. Papageorgiou et al. (2021). Deep networks for direction-of-arrival estimation in low SNR, IEEE Transactions on Signal Processing 
	% multilayer classification. Deep learning models typically require large amounts of labeled data for training. Acquiring and labeling such extensive datasets can be resource-intensive and time-consuming.
\end{itemize}

Hybrid models:
\begin{itemize}
	\item Elbir, A. M. (2020). DeepMUSIC: Multiple signal classification via deep learning, IEEE Sensors Letters 
	% learn spectrum instead of angles. The partitioning approach, while useful for managing complexity, can struggle with closely spaced targets that might fall into different subregions, potentially complicating accurate DOA estimation for such cases.
	\item Shmuel et al. (2023). Deep Root MUSIC algorithm for data-driven DoA estimation, International Conference on Acoustics, Speech and Signal Processing 2023
	% Good performance. Only for ULA
	\item Shmuel et al. (2023). SubspaceNet: Deep learning-aided subspace methods for doa estimation, arXiv preprint
	% Good performance. Only for ULA
\end{itemize}

\end{frame}


\begin{frame}
\frametitle{}
\centering
\Large
Thank you for your attention
\end{frame}

\end{document}